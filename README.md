# vfd_setting.odt

These are the recommended settings for the Huanyang VFD when used with a high speed spindle (24000RPM). Speed control is done via a 0-10V analog signal provided by an external source (BOB). The settings can be changed to support RS285 connection to the PC for MODBUS control.

# cnc_wiring.odt

A series of tables showing the connections of various components (BOB, stepper driver, VFD) and the connections between them. A circuit diagram should be produced one day.

